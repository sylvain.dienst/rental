package app.statement.abstractstatement;

public interface IStatement {
    String statement();
}
