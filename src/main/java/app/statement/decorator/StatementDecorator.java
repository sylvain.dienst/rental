package app.statement.decorator;

import app.Customer;
import app.Rental;
import app.statement.abstractstatement.AStatement;
import java.util.List;

public abstract class StatementDecorator extends AStatement {

    protected AStatement statement;

    protected StatementDecorator(List<Rental> rentals, Customer customer) {
        super(rentals, customer);
    }
}
