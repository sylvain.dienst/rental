package app.statement.decorating;

import app.Rental;
import app.statement.abstractstatement.AStatement;
import app.statement.decorator.StatementDecorator;

public class ConsoleStatement extends StatementDecorator {

    public ConsoleStatement(AStatement statement) {
        super(statement.getRentals(), statement.getCustomer());
        super.statement = statement;
    }

    public String statement() {
        statement.statement();


        StringBuilder resultBuilder = new StringBuilder("Rental Record for " + customer.getName() + "\n");
        for (Rental rent : this.rentals ) {
            resultBuilder
                    .append("\t")
                    .append(rent.getMovie().getTitle())
                    .append("\t")
                    .append(rent.getMovie().getPrice(rent.getDaysRented()))
                    .append("\n");
        }

        resultBuilder
                .append("Amount owed is ")
                .append(customer.getTotalRentalPrice())
                .append("\n");

        resultBuilder.append("You earned ")
                .append(customer.getTotalFrequentPoint())
                .append(" frequent renter points.");

        return resultBuilder.toString();

    }

}
