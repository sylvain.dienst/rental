package app;

import app.statement.abstractstatement.AStatement;
import app.statement.decorating.ConsoleStatement;
import app.statement.decorating.HtmlStatement;
import app.statement.Statement;

import java.util.*;



public class Customer {


    private final java.lang.String name;

    private final ArrayList<Rental> rentals;

    private AStatement statement;


    public Customer(String name) {
        this.name = name;
        this.rentals = new ArrayList<Rental>();
    }


    public void addRentals(Rental arg) {

        rentals.add(arg);

    }

    public List<Rental> getRentals()
    {
        return this.rentals;
    }

    public java.lang.String getName() {

        return name;

    }

    public String statement()
    {
        this.statement = new ConsoleStatement(new Statement(this.rentals, this));
        return this.statement.statement();
    }

    public String htmlStatement()
    {
        this.statement = new HtmlStatement(new Statement(this.rentals, this));
        return this.statement.statement();
    }

    public double getTotalRentalPrice()
    {
        double total = 0;
        for (Rental rent :
                this.rentals) {
            total+=rent.getMovie().getPrice(rent.getDaysRented());
        }
        return total;
    }

    public int getTotalFrequentPoint()
    {
        int total = 0;
        for (Rental rent :
                this.rentals) {
            total+=rent.getMovie().getFrequentPoint();
        }
        return total;
    }

}