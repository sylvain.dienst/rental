package app.movie;

import app.movie.movieclass.abstractmovie.IMovieClass;

public class Movie {

    private IMovieClass movieCLass;

    public Movie(IMovieClass mcl)
    {
        movieCLass = mcl;
    }

    public IMovieClass getMovieCLass()
    {
        return movieCLass;
    }

    public void setMovieCLass(IMovieClass movieCLass)
    {
        this.movieCLass = movieCLass;
    }

    public double getPrice(Integer daysRented)
    {
        return this.movieCLass.getPrice(daysRented);
    }

    public int getFrequentPoint()
    {
        return this.movieCLass.getFrequentPoint();
    }

    public void setFrequentPoint(Integer frequentPoint)
    {
        this.movieCLass.setFrequentPoint(frequentPoint);
    }

    public String getTitle() {
        return this.movieCLass.getTitle();
    }

    public void setTitle(String title)
    {
        this.movieCLass.setTitle(title);
    }
}