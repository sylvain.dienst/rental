package app.movie.movieclass;

import app.movie.movieclass.abstractmovie.AMovie;

public class RegularMovie extends AMovie {

    public RegularMovie(String title) {
        super(2, 2, 1.5, 1, title);
    }
}
