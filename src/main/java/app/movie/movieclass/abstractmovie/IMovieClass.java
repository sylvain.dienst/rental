package app.movie.movieclass.abstractmovie;

public interface IMovieClass {

    double getPrice(Integer daysRented);

    int getFrequentPoint();

    void setFrequentPoint(Integer frequentPoint);

    String getTitle();

    void setTitle(String title);
}
