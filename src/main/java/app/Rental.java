package app;

import app.movie.Movie;

public class Rental {

    private final int daysRented;

    private final Movie movie;


    public Rental(Movie movie, int daysRented) {

        this.movie = movie;

        this.daysRented = daysRented;

    }

    public int getDaysRented() {

        return daysRented;

    }

    public Movie getMovie() {

        return movie;

    }

}