package app;

import app.movie.Movie;
import app.movie.movieclass.ChildrenMovie;
import app.movie.movieclass.NewReleaseMovie;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerTest {
    Customer customer;
    Rental r1;
    Rental r2;
    @Before
    public void setUp() throws Exception {
        this.customer = new Customer("test");
        this.r1 = new Rental(new Movie(new ChildrenMovie("Enfant")),5);
        this.customer.addRentals(r1);
        this.r2 = new Rental(new Movie(new NewReleaseMovie("New")),10);
        this.customer.addRentals(r2);
    }

    @Test
    public void getName() {
        assertEquals("test", this.customer.getName());
    }

    @Test
    public void getTotalRentalPrice() {
        assertEquals(24.5, this.customer.getTotalRentalPrice(), .1);
    }

    @Test
    public void getTotalFrequentPoint() {
        assertEquals(3, this.customer.getTotalFrequentPoint());
    }

    @Test
    public void getRental()
    {
        ArrayList list = new ArrayList<Rental>();
        list.add(this.r1);
        list.add(this.r2);
        assertArrayEquals(list.toArray(), customer.getRentals().toArray());
    }
}