package app.statement.decorating;

import app.Customer;
import app.Rental;
import app.movie.Movie;
import app.movie.movieclass.ChildrenMovie;
import app.movie.movieclass.RegularMovie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConsoleStatementTest {
    Customer c1;

    @Before
    public void setUp() throws Exception {
        this.c1 = new Customer("Sylvain");
        c1.addRentals(new Rental(new Movie(new ChildrenMovie("Film enfant")), 5));
        c1.addRentals(new Rental(new Movie(new RegularMovie("Film normal")), 5));
    }

    @Test
    public void statement()
    {
        StringBuilder resultBuilder = new StringBuilder("Rental Record for Sylvain\n\tFilm enfant\t4.5\n\tFilm normal\t6.5\nAmount owed is 11.0\nYou earned 2 frequent renter points.");
        assertEquals(resultBuilder.toString(), this.c1.statement());
    }
}